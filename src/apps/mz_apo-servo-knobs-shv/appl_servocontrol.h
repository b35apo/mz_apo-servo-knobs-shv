#ifndef SERVOCONTROL_H
#define SERVOCONTROL_H

int servops2_set_pos_raw(int servo, int val);
int servops2_get_pos_raw(int servo);
double servo_get_pos(int servo);
int servo_set_pos(int servo, double val);

#endif // SERVOCONTROL_H
