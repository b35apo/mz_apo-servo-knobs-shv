#include <stdio.h>

#include "shv_tree.h"
#include "shv_methods.h"
#include "ulut/ul_utdefs.h"

#include "appl_shv.h"
#include "appl_servocontrol.h"

double servo_pos_val[3];

typedef struct items_map_to_servo {
  const char *name;
  double *val_ptr;
} items_map_to_servo_t;

const items_map_to_servo_t items_map_to_servo[] = {
  {.name = "knob_r", .val_ptr = (void *)0},
  {.name = "knob_g", .val_ptr = (void *)1},
  {.name = "knob_b", .val_ptr = (void *)2},
};

int shv_servo_double_val_set(shv_con_ctx_t * shv_ctx, shv_node_t* item, int rid);
int shv_servo_double_val_get(shv_con_ctx_t * shv_ctx, shv_node_t* item, int rid);

const shv_method_des_t shv_servo_double_val_dmap_item_get = {
  .name = "get", .method = shv_servo_double_val_get
};

const shv_method_des_t shv_servo_double_val_dmap_item_set = {
  .name = "set", .method = shv_servo_double_val_set
};

const shv_method_des_t shv_servo_double_val_dmap_item_type = {
  .name = "typeName", .method = shv_type
};

const shv_method_des_t * const shv_servo_double_val_items[] = {
  &shv_dmap_item_dir,
  &shv_servo_double_val_dmap_item_get,
  &shv_dmap_item_ls,
  &shv_servo_double_val_dmap_item_set,
  &shv_servo_double_val_dmap_item_type,
};

const shv_dmap_t shv_servo_double_val_dmap = {
  .methods = {.items = (void **)shv_servo_double_val_items,
  .count = sizeof(shv_servo_double_val_items)/sizeof(shv_servo_double_val_items[0]),
  .alloc_count = 0,
}};

/****************************************************************************/

int shv_servo_device_type(shv_con_ctx_t * shv_ctx, shv_node_t* item, int rid);

const shv_method_des_t shv_dev_root_dmap_item_device_type = {
  .name = "deviceType", .method = shv_servo_device_type
};

const shv_method_des_t * const shv_dev_root_dmap_items[] = {
  &shv_dev_root_dmap_item_device_type,
  &shv_dmap_item_dir,
  &shv_dmap_item_ls,
};

const shv_dmap_t shv_dev_root_dmap = {
  .methods = {.items = (void **)shv_dev_root_dmap_items,
  .count = sizeof(shv_dev_root_dmap_items)/sizeof(shv_dev_root_dmap_items[0]),
  .alloc_count = 0,
}};

/****************************************************************************
 * Name: shv_servo_device_type
 *
 * Description:
 *   Method "set".
 *
 ****************************************************************************/

int shv_servo_device_type(shv_con_ctx_t * shv_ctx, shv_node_t* item, int rid)
{
  const char *str = "mzapoknobs";

  shv_unpack_data(&shv_ctx->unpack_ctx, 0, 0);

  shv_send_str(shv_ctx, rid, str);

  return 0;
}

/****************************************************************************
 * Name: shv_servo_double_val_set
 *
 * Description:
 *   Method "set".
 *
 ****************************************************************************/

int shv_servo_double_val_set(shv_con_ctx_t * shv_ctx, shv_node_t* item, int rid)
{
  double val;

  shv_unpack_data(&shv_ctx->unpack_ctx, 0, &val);

  shv_node_typed_val_t *item_node = UL_CONTAINEROF(item, shv_node_typed_val_t,
                                               shv_node);
  int servo = (intptr_t)item_node->val_ptr;
  servo_set_pos(servo, val);

  fprintf(stderr, "INFO: servo %d set to %e\n", servo, val);

  val = servo_get_pos(servo);
  shv_send_double(shv_ctx, rid, val);

  return 0;
}


/****************************************************************************
 * Name: shv_servo_double_val_get
 *
 * Description:
 *   Method "get".
 *
 ****************************************************************************/

int shv_servo_double_val_get(shv_con_ctx_t * shv_ctx, shv_node_t* item, int rid)
{
  double val;

  shv_unpack_data(&shv_ctx->unpack_ctx, 0, 0);

  shv_node_typed_val_t *item_node = UL_CONTAINEROF(item, shv_node_typed_val_t,
                                               shv_node);

  int servo = (intptr_t)item_node->val_ptr;
  val = servo_get_pos(servo);

  fprintf(stderr, "INFO: servo %d get %e\n", servo, val);

  shv_send_double(shv_ctx, rid, val);

  return 0;
}

/****************************************************************************
 * Name: shv_tree_create
 *
 * Description:
 *  Initialize and fill the SHV tree with blocks and parameters.
 *
 ****************************************************************************/

shv_node_t *shv_tree_create(void)
{
  shv_node_t *tree_root;
  shv_node_t *item;
  shv_node_t *item_parent;
  shv_node_typed_val_t *item_val;
  const items_map_to_servo_t *ims_ptr = items_map_to_servo;
  int ims_cnt = sizeof(items_map_to_servo)/sizeof(*items_map_to_servo);

  int mode = 0;

  tree_root = shv_tree_node_new("", &shv_dev_root_dmap, 0);
  if (tree_root == NULL) {
    fprintf(stderr, "ERROR: shv_tree_node_new failed\n");
    return NULL;
  }

  /* Create children for SHV input and output blocks */

  for (int i = 0; i < ims_cnt; i++) {

    item = shv_tree_node_new(ims_ptr[i].name, &shv_dir_ls_dmap, mode);
    if (item == NULL)
    {
      fprintf(stderr, "ERROR: Failed to allocate memory for SHV tree node \"%s\"!\n", ims_ptr[i].name);
      return NULL;
    }

    shv_tree_add_child(tree_root, item);

    item_parent = item;

    item_val = shv_tree_node_typed_val_new("value", &shv_servo_double_val_dmap, mode);
    if (item_val == NULL)
    {
      fprintf(stderr, "ERROR: Failed to allocate memory for SHV tree block \"value\"!\n");
      return NULL;
    }

    item_val->type_name = "double";
    item_val->val_ptr = ims_ptr[i].val_ptr;

    shv_tree_add_child(item_parent, &item_val->shv_node);
  }

  return tree_root;
}

/****************************************************************************
 * Name: shv_tree_init
 *
 * Description:
 *  Entry point for SHV related operations. Calls shv_tree_create to create
 *  a SHV tree and then initialize SHV connection.
 *
 ****************************************************************************/

shv_con_ctx_t *shv_tree_init(void)
{
  shv_node_t *tree_root;

  tree_root = shv_tree_create();
  if (tree_root == NULL) {
    fprintf(stderr, "ERROR: shv_tree_create() failed.\n");
    return NULL;
  }

  /* Initialize SHV connection */

  shv_con_ctx_t *ctx = shv_com_init(tree_root);
  if (ctx == NULL) {
    fprintf(stderr, "ERROR: shv_init() failed.\n");
    return NULL;
  }

  return ctx;
}

