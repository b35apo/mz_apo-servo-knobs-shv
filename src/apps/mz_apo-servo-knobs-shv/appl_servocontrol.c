#include "appl_servocontrol.h"
#include "mzapo_regs.h"
#include "mzapo_phys.h"
#include <stdint.h>
#include <limits.h>
#include <stdio.h>

#define SERVOPS2_PWM_MIN       80000
#define SERVOPS2_PWM_MAX      200000
#define SERVOPS2_PWM_PERIOD  1000000
#define SERVOPS2_SERVO_COUNT       4

static const uint32_t servops2_pwm_offsets[SERVOPS2_SERVO_COUNT] ={
  SERVOPS2_REG_PWM1_o,
  SERVOPS2_REG_PWM2_o,
  SERVOPS2_REG_PWM3_o,
  SERVOPS2_REG_PWM4_o
};

uint32_t servops2_cr_mode = 0x10f;

void *servops2_base = NULL;

int servops2_init_if_needed(void)
{
  if (servops2_base == NULL)
    servops2_base = map_phys_address(SERVOPS2_REG_BASE_PHYS, SERVOPS2_REG_SIZE, 0);

  if (servops2_base == NULL) {
    fprintf(stderr, "ERROR: servops2 base cannot be mapped\n");
    return -1;
  }

  if ((*(volatile uint32_t *)(servops2_base + DCSPDRV_REG_CR_o) != servops2_cr_mode) ||
      (*(volatile uint32_t *)(servops2_base + SERVOPS2_REG_PWMPER_o) != SERVOPS2_PWM_PERIOD)) {
    for (int i = 0; i < SERVOPS2_SERVO_COUNT; i++) {
      uint32_t pwm_duty = *(volatile uint32_t *)(servops2_base + servops2_pwm_offsets[i]);
      if ((pwm_duty < SERVOPS2_PWM_MIN) || (pwm_duty > SERVOPS2_PWM_MAX))
        *(volatile uint32_t *)(servops2_base + servops2_pwm_offsets[i]) = 0;
    }
    *(volatile uint32_t *)(servops2_base + DCSPDRV_REG_CR_o) = servops2_cr_mode;
    *(volatile uint32_t *)(servops2_base + SERVOPS2_REG_PWMPER_o) = SERVOPS2_PWM_PERIOD;

    return 1;
  } else {
    return 0;
  }
}

int servops2_set_pos_raw(int servo, int val)
{
    if (servo < 0 || servo >= SERVOPS2_SERVO_COUNT)
        return -1;

    if (val < SERVOPS2_PWM_MIN)
        val = SERVOPS2_PWM_MIN;
    if (val > SERVOPS2_PWM_MAX)
        val = SERVOPS2_PWM_MAX;

    if (servops2_init_if_needed() < 0)
      return -1;

    *(volatile uint32_t *)(servops2_base + servops2_pwm_offsets[servo]) = val;

   return val;
}

int servops2_get_pos_raw(int servo)
{
  if (servo < 0 || servo >= SERVOPS2_SERVO_COUNT)
    return -1;

  if (servops2_init_if_needed() < 0)
    return -1;

  return *(volatile uint32_t *)(servops2_base + servops2_pwm_offsets[servo]);
}

double servo_get_pos(int servo)
{
  double d;
  int raw;

  raw = servops2_get_pos_raw(servo);

  if (raw < 0)
    return 0.0;

  d = raw - SERVOPS2_PWM_MIN;
  d /= SERVOPS2_PWM_MAX - SERVOPS2_PWM_MIN;
  d *= 100;

  return d;
}

int servo_set_pos(int servo, double val)
{
    double d = val / 100.0;
    d *= SERVOPS2_PWM_MAX - SERVOPS2_PWM_MIN;
    d += SERVOPS2_PWM_MIN;
    if (d > INT_MAX)
        d = INT_MAX;
    if (d < INT_MIN)
        d = INT_MIN;
    return servops2_set_pos_raw(servo, (int)d);
}
